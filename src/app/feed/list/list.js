(function(angular) {
  'use strict';

  var module = angular.module('flickerDemo.feed.list', [
    'common.directives.dynamicTruncate'
  ]);

  module.controller('Feed.List', function($scope, $route, publicFeedCollection) {

    $scope.tags = '';

    if ($route.current.params.hasOwnProperty('tags')) {
      $scope.tags = $route.current.params.tags;
    }

    $scope.showByTags = function() {
      $route.updateParams({tags: $scope.tags});
    };

    publicFeedCollection.loadByTags($scope.tags).then(function(data) {
      $scope.collection = data;
    });

  });

})(window.angular);