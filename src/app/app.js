(function(angular) {
  'use strict';

  var app = angular.module('flickerDemo', [
    'ngRoute',
    'truncate',
    'common.filters.default',
    'common.directives.publishedDate',
    'common.directives.windowResize',
    'flickerDemo.feed'
  ]);

  app.config(['$routeProvider',
    function($routeProvider) {
      $routeProvider.
        when('/', {
          templateUrl: 'feed/list/list.html',
          controller: 'Feed.List'
        });
    }]);

})(window.angular);