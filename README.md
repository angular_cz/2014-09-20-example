# Angular.cz devstack #

Devstack for simple start with AngularJS by Angular.cz

### Using ###

* Clone repository
* run 
```
npm install
gulp devel
```

### Gulp tasks ###

**devel**

* run application from source codes, run build-in development server with watching and livereload

**build**

* make minified application in build directory

Testing

```
# karma
npm run test

# protractor
npm run protractor
```
